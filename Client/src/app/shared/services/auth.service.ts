import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {User} from '../interfaces';
import {tap} from 'rxjs/operators';
import * as moment from 'moment';
import {Moment} from 'moment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private token: string = null;

  private expiresAt: string = null;

  constructor(private http: HttpClient) {
  }

  registration(user: User): Observable<User> {
    return this.http.post<User>('/api/registration', user);
  }

  login(user: User): Observable<any> {
    return this.http.post('/api/login', user)
      .pipe(
        tap(
          ({token, expiresAt}) => {
            localStorage.setItem('auth-token', token);
            localStorage.setItem('expiresAt', JSON.stringify(expiresAt.valueOf()));
            this.setToken(token);
            this.setExpiresDate(JSON.stringify(expiresAt.valueOf()));
          }
        )
      );
  }

  setToken(token: string) {
    this.token = token;
  }

  getToken(): string {
    return localStorage.getItem('auth-token');
    // return this.token;
  }

  setExpiresDate(expiresAt: string) {
    this.expiresAt = expiresAt;
  }

  getExpiresDate(): Moment {
    return moment(JSON.parse(localStorage.getItem('expiresAt')));
    // return moment(JSON.parse(this.expiresAt));
  }

  isAuthenticated(): boolean {
    return !!this.getToken() && moment().isBefore(this.getExpiresDate());
  }

  logout() {
    this.setToken(null);
    this.setExpiresDate(null);
    localStorage.removeItem('auth-token');
    localStorage.removeItem('expiresAt');
  }

  /*public isLoggedIn() {
    return moment().isBefore(this.getExpiration());
  }

  getExpiration() {
    const expiration = localStorage.getItem('expiresAt');
    const expiresAt = new Date(expiration);
    console.log('expiresAt: ', expiresAt);
    console.log('moment(expiresAt): ', moment(expiresAt));
    return moment(expiration);
  }*/

}
