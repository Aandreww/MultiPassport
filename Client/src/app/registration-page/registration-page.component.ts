import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormControl, FormGroup} from '@angular/forms';
import {AuthService} from '../shared/services/auth.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-registration-page',
  templateUrl: './registration-page.component.html',
  styleUrls: ['./registration-page.component.css']
})
export class RegistrationPageComponent implements OnInit, OnDestroy {

  form: FormGroup;
  aSub: Subscription;

  constructor(
    private auth: AuthService,
    private router: Router) {
  }

  ngOnInit() {
    this.form = new FormGroup({
      username: new FormControl(null),
      password: new FormControl(null),
      firstName: new FormControl(null),
      lastName: new FormControl(null)
    });
  }

  ngOnDestroy() {
    if (this.aSub) {
      this.aSub.unsubscribe();
    }
  }

  onSubmit() {
    this.form.disable();

    console.log(this.form.value);

    this.aSub = this.auth.registration(this.form.value)
    // TODO: Вспомнить ансабскрайбы
    /*.pipe(
        takeUntil()
      )*/
      .subscribe(
        () => {
          this.router.navigate(['/main']);
        }/*,
      error => {
        this.form.enable();
      }*/
      );
  }

}
