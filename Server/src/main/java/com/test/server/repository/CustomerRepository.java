package com.test.server.repository;

import com.test.server.models.Customer;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface CustomerRepository extends MongoRepository<Customer, Long> {

    Customer findByUsername(String username);

}
